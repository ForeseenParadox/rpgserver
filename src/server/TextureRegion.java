package server;

public class TextureRegion
{

	private Texture texture;
	private int x;
	private int y;
	private int width;
	private int height;

	public TextureRegion(Texture t, int x, int y, int width, int height)
	{
		this.texture = t;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public Texture getTexture()
	{
		return texture;
	}

	public void setTexture(Texture texture)
	{
		this.texture = texture;
	}

	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public int getRegionWidth()
	{
		return width;
	}

	public void setRegionWidth(int width)
	{
		this.width = width;
	}

	public int getRegionHeight()
	{
		return height;
	}

	public void setRegionHeight(int height)
	{
		this.height = height;
	}

}
