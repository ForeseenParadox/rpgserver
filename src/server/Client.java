package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.common.primitives.Bytes;

import draconic.rpggame.item.InventoryListener;
import draconic.rpggame.item.Item;
import draconic.rpggame.net.FileTransfer;
import draconic.rpggame.net.PacketType;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.entity.Entity;
import draconic.rpggame.world.entity.Player;
import server.entity.ServerEntityFactory;
import server.entity.ServerPlayer;
import server.tiled.TileSet;

public class Client
{

	private Socket socket;
	private DataInputStream dataIn;
	private DataOutputStream dataOut;
	private String user;
	private Thread listenerThread;
	private ServerPlayer player;
	private boolean connected;

	public Client(Socket socket)
	{
		this("NA", socket);
	}

	public Client(String user, Socket socket)
	{
		try
		{
			this.user = user;
			this.socket = socket;
			this.connected = true;
			this.dataOut = new DataOutputStream(socket.getOutputStream());
			this.dataIn = new DataInputStream(socket.getInputStream());
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}

		this.listenerThread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					while (connected)
					{
						int packetId = dataIn.readInt();
						// System.out.println("received: " + packetId);
						handlePacket(packetId);
					}
				} catch (IOException e)
				{
					System.out.println(e.getMessage() + ": " + getIp() + ":" + getPort());
					if (connected)
					{
						try
						{
							removeClient();
						} catch (IOException e1)
						{
							e1.printStackTrace();
						}
					}
				}
			}
		});

		listenerThread.start();
	}


	public boolean isConnected()
	{
		return connected;
	}

	public String getUser()
	{
		return user;
	}

	public Player getPlayer()
	{
		return player;
	}

	public Thread getListenerThread()
	{
		return listenerThread;
	}

	public InetAddress getIp()
	{
		return getSocket().getInetAddress();
	}

	public int getPort()
	{
		return getSocket().getPort();
	}

	public Socket getSocket()
	{
		return socket;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public void setSocket(Socket socket)
	{
		this.socket = socket;
	}

	public DataOutputStream getDataOut()
	{
		return dataOut;
	}

	public void sendMessage(String message)
	{
		try
		{
			dataOut.writeInt(PacketType.MESSAGE);
			dataOut.writeUTF(message);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void handlePacket(int packetId)
	{
		try
		{
			if (packetId == PacketType.PLAYER_INFO)
			{
				user = dataIn.readUTF();

				// send static world data to client
				ServerWorld w = Server.getInstance().getServerWorld();

				// write the name and amount of tileset resources
				dataOut.writeUTF(w.getWorldName());
				dataOut.writeInt(w.getMap().getTilesets().size());

				// transfer the world file
				FileTransfer.sendFile(new File(w.getMap().getLocation()), dataOut);

				// transfer all of the tileset resources
				for (TileSet set : w.getMap().getTilesets())
				{
					File file = new File(set.getTexture().getPath());
					FileTransfer.sendFile(file, dataOut);
				}

				// init player
				player = new ServerPlayer(w, new Vector2f(w.getSpawnPoint()));
				player.assignSerialId(Server.getInstance().getEntityIdGen().allocateId());
				w.addEntity(player);

				player.getInventory().addListener(new InventoryListener()
				{

					@Override
					public void itemsAdded(Item item, int amount, int slot)
					{
						try
						{
							dataOut.writeInt(PacketType.INVENTORY_ADD);
							dataOut.writeLong(player.getSerialId());
							dataOut.writeInt(item.getId());
							dataOut.writeInt(amount);
						} catch (IOException e)
						{
							e.printStackTrace();
						}
					}

					@Override
					public void itemsRemoved(Item item, int amount, int slot)
					{

					}

					@Override
					public void slotCleared(int slot)
					{
						try
						{
							dataOut.writeInt(PacketType.INVENTORY_SLOT_CLEARED);
							dataOut.writeLong(player.getSerialId());
							dataOut.writeInt(slot);
						} catch (IOException e)
						{
							e.printStackTrace();
						}
					}
				});

				// send player information packet to new client
				player.serialize(dataOut, PacketType.PLAYER_INFO);

				// send message
				Server.getInstance().broadcastMessage(user + " has connected");

				// add the client to the server
				Server.getInstance().addClient(this);

			} else if (packetId == PacketType.ENTITY_UPDATE)
			{
				long id = dataIn.readLong();
				// skip the type int
				dataIn.readInt();
				Server.getInstance().getServerWorld().getEntity(id).deserialize(dataIn);
			} else if (packetId == PacketType.DISCONNECT)
			{
				removeClient();
			} else if (packetId == PacketType.MESSAGE)
			{
				String message = dataIn.readUTF();
				// broadcast packet to all clients
				Server.getInstance().getGlobalDataOut().writeInt(PacketType.MESSAGE);
				Server.getInstance().getGlobalDataOut().writeUTF(getUser() + ": " + message);
			} else if (packetId == PacketType.ENTITY_ADD)
			{
				// skip the id that the client sends
				dataIn.readLong();
				int typeId = dataIn.readInt();
				Entity e = ServerEntityFactory.createEntity(typeId, Server.getInstance().getServerWorld());
				if (e != null)
				{
					// assign new id
					e.deserialize(dataIn);
					Server.getInstance().getServerWorld().addEntity(e);
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void removeClient() throws IOException
	{
		connected = false;

		Server.getInstance().removeClient(this);

		// send message
		Server.getInstance().broadcastMessage(user + " has disconnected");

		// close the socket if an error hasn't closed it already
		if (!socket.isClosed())
			socket.close();
	}

}
