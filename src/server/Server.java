package server;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import draconic.rpggame.net.PacketType;

public class Server
{

	private static volatile boolean running;
	private static Server instance;

	private ServerSocket serverSocket;
	private Thread connectionListener;
	private volatile List<Client> clients;
	private volatile List<Client> pendingClientsToAdd;
	private volatile List<Client> pendingClientsToRemove;

	private ServerWorld serverWorld;
	private Thread worldThread;

	private ServerProperties properties;

	private ByteArrayOutputStream globalByteArray;
	private DataOutputStream globalDataOut;

	private IdGen entityIdGen;

	public Server(String propertiesLocation)
	{
		this(propertiesLocation, 0);
	}

	public Server(String propertiesLocation, int port)
	{
		instance = this;
		try
		{
			serverSocket = new ServerSocket(port);
			properties = new ServerProperties(propertiesLocation);
			clients = new ArrayList<Client>();
			pendingClientsToAdd = Collections.synchronizedList(new ArrayList<Client>());
			pendingClientsToRemove = Collections.synchronizedList(new ArrayList<Client>());
			globalByteArray = new ByteArrayOutputStream();
			globalDataOut = new DataOutputStream(globalByteArray);
			entityIdGen = new IdGen();

			// initialize connection thread
			connectionListener = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					while (running)
					{
						try
						{
							Socket socket = serverSocket.accept();
							System.out.println("Established connection: " + socket.getInetAddress().getHostAddress() + ":" + socket.getPort());
							new Client(socket);
						} catch (IOException e)
						{
							System.err.println("Error accepting socket: ");
							e.printStackTrace();
						}
					}
				}
			});

		} catch (IOException e)
		{
			System.err.println("Server could not properly start:");
			e.printStackTrace();
		}
	}

	public ServerProperties getProperties()
	{
		return properties;
	}

	public IdGen getEntityIdGen()
	{
		return entityIdGen;
	}

	public ServerWorld getServerWorld()
	{
		return serverWorld;
	}

	public ServerSocket getServerSocket()
	{
		return serverSocket;
	}

	public DataOutputStream getGlobalDataOut()
	{
		return globalDataOut;
	}

	public InetAddress getLocalAddress()
	{
		return ((InetSocketAddress) serverSocket.getLocalSocketAddress()).getAddress();
	}

	public int getLocalPort()
	{
		return serverSocket.getLocalPort();
	}

	public List<Client> getClients()
	{
		return clients;
	}

	public void broadcastMessage(String message)
	{
		try
		{
			globalDataOut.writeInt(PacketType.MESSAGE);
			globalDataOut.writeUTF(message);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void run()
	{
		// initialize server variables
		running = true;

		// start the connection listener thread
		connectionListener.start();

		System.out.println("Server running on port " + serverSocket.getLocalPort());

		// load world
		serverWorld = (ServerWorld) new ServerWorldLoader().loadWorld(properties.getWorldName(), properties.getWorldLocation());
		worldThread = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				double last = System.currentTimeMillis();
				double now = System.currentTimeMillis();
				double[] timers = new double[2];
				double serverUpdateTime = 1000.0 / 60.0;
				double clientUpdateTime = 1000.0 / 60.0;

				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				DataOutputStream output = new DataOutputStream(outputStream);
				while (running)
				{
					now = System.currentTimeMillis();
					for (int i = 0; i < timers.length; i++)
						timers[i] += now - last;
					last = System.currentTimeMillis();

					if (timers[0] < serverUpdateTime)
					{
						double possibleSleepTime = serverUpdateTime - timers[0];

						try
						{
							Thread.sleep((long) possibleSleepTime);
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					// check for new clients
					removeClients();
					addNewClients();

					while (timers[0] >= serverUpdateTime)
					{
						// update server world
						serverWorld.update((float) timers[0]);
						timers[0] -= serverUpdateTime;
					}

					if (timers[1] >= clientUpdateTime)
					{
						// update clients world
						serverWorld.serializeEntities(output, PacketType.ENTITY_UPDATE);
						byte[] entityData = outputStream.toByteArray();
						byte[] globalData = globalByteArray.toByteArray();

						// send global and entity data
						sendGlobalData(entityData);
						sendGlobalData(globalData);

						// reset globals
						outputStream.reset();
						globalByteArray.reset();

						timers[1] = 0;
					}
				}

				// send any last global data
				sendGlobalData(globalByteArray.toByteArray());
			}

		});
		worldThread.start();

	}

	public void sendGlobalData(byte[] data)
	{
		for (int i = 0; i < clients.size(); i++)
		{
			Client client = clients.get(i);
			try
			{
				if (client.isConnected())
					client.getDataOut().write(data);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void addClient(Client client)
	{
		pendingClientsToAdd.add(client);
	}

	public void removeClient(Client client)
	{
		pendingClientsToRemove.add(client);
	}

	private void addNewClients()
	{
		for (Client pendingClient : pendingClientsToAdd)
		{

			for (Client connectedClient : clients)
			{
				if (connectedClient != pendingClient)
				{
					try
					{
						// send pending client information to connected client
						connectedClient.getDataOut().writeInt(PacketType.PLAYER_JOIN);
						connectedClient.getDataOut().writeUTF(pendingClient.getUser());
						connectedClient.getDataOut().writeLong(pendingClient.getPlayer().getSerialId());

						// send connected client information to pending client
						pendingClient.getDataOut().writeInt(PacketType.PLAYER_JOIN);
						pendingClient.getDataOut().writeUTF(connectedClient.getUser());
						pendingClient.getDataOut().writeLong(connectedClient.getPlayer().getSerialId());
					} catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}

			clients.add(pendingClient);

			// add the entities to the server world
			// serverWorld.addPendingEntity(pendingClient.getPlayer());
			// send all world entities to this client
			serverWorld.serializeEntities(pendingClient.getDataOut(), PacketType.ENTITY_ADD);
			// send client MOTD message
			pendingClient.sendMessage(properties.getMotd() + "[WHITE]");

		}
		pendingClientsToAdd.clear();

	}

	private void removeClients()
	{
		clients.removeAll(pendingClientsToRemove);

		for (Client pendingClient : pendingClientsToRemove)
		{
			// remove the client player from the world
			if (pendingClient.getPlayer() != null)
				serverWorld.removeEntity(pendingClient.getPlayer());
		}

		pendingClientsToRemove.clear();

	}

	public void stop()
	{
		System.out.println("Closing server...");
		try
		{
			// send disconnect packet to all clients
			getGlobalDataOut().writeInt(PacketType.DISCONNECT);
			running = false;
			worldThread.join();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("Server closed");
	}

	public static boolean isRunning()
	{
		return running;
	}

	public static Server getInstance()
	{
		return instance;
	}

	public static void main(String[] args)
	{
		new Server("server.properties", 25565).run();
	}

}
