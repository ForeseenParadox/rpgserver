package server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class ServerProperties
{

	private int port;
	private String worldName;
	private String worldLocation;
	private String motd;

	public ServerProperties(String loc)
	{
		// load server properties file
		try
		{
			File propertiesFile = new File(loc);
			if (!propertiesFile.exists())
			{
				if (propertiesFile.getParentFile() != null)
				{
					if (!propertiesFile.getParentFile().exists())
						propertiesFile.mkdirs();
				}
				propertiesFile.createNewFile();
				System.err.println("Server properties file not found, creating new file.");
				BufferedWriter writer = new BufferedWriter(new FileWriter(propertiesFile));
				writer.write("port=25565\n");
				writer.write("worldName=world\n");
				writer.write("worldLocation=Worlds/world/map.tmx\n");
				writer.write("motd=Welcome to the server!\n");
				writer.close();
			}

			// read properties
			Properties props = new Properties();

			FileInputStream inputStream = new FileInputStream(propertiesFile);
			props.load(inputStream);
			inputStream.close();

			port = Integer.parseInt(props.getProperty("port"));
			worldName = props.getProperty("worldName");
			worldLocation = props.getProperty("worldLocation");
			motd = props.getProperty("motd");
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public int getPort()
	{
		return port;
	}

	public String getWorldName()
	{
		return worldName;
	}

	public String getWorldLocation()
	{
		return worldLocation;
	}

	public String getMotd()
	{
		return motd;
	}

}
