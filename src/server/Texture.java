package server;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Texture
{

	private String path;
	private int width;
	private int height;

	public Texture(String path)
	{
		this.path = path;
		try
		{
			BufferedImage result = ImageIO.read(new File(path));
			width = result.getWidth();
			height = result.getHeight();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	//
	// public Texture(int width, int height)
	// {
	// this.width = width;
	// this.height = height;
	// }

	public String getPath()
	{
		return path;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

}
