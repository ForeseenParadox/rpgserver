package server;

import java.util.ArrayList;
import java.util.List;

public class TextureAnimation
{

	private List<TextureRegion> animationFrames;
	private int currentFrame;

	private long animationDelay;
	private long lastTextureSwitch;

	private boolean started;

	public TextureAnimation(Texture parent, int startX, int startY, int frameWidth, int frameHeight, int framesWide, int framesTall, long delay)
	{
		this(parent, startX, startY, frameWidth, frameHeight, framesWide, framesTall, null, delay);
	}

	public TextureAnimation(Texture parent, int startX, int startY, int frameWidth, int frameHeight, int framesWide, int framesTall, int[] indices, long delay)
	{

		List<TextureRegion> unindexedFrames = new ArrayList<TextureRegion>();
		for (int y = startY; y < startY + (frameHeight * framesTall); y += frameHeight)
		{
			for (int x = startX; x < startX + (frameWidth * framesWide); x += frameWidth)
			{
				TextureRegion frame = new TextureRegion(parent, x, y, frameWidth, frameHeight);
				unindexedFrames.add(frame);
			}
		}

		if (indices == null)
			animationFrames = unindexedFrames;
		else
		{
			animationFrames = new ArrayList<TextureRegion>();
			for (int i : indices)
				animationFrames.add(unindexedFrames.get(i));
		}

		animationDelay = delay;

		lastTextureSwitch = System.currentTimeMillis();

		started = true;
	}

	public TextureAnimation(long delta)
	{
		animationFrames = new ArrayList<TextureRegion>();
		currentFrame = 0;
		lastTextureSwitch = System.currentTimeMillis();
		started = true;
	}

	public List<TextureRegion> getAnimationFrames()
	{
		return animationFrames;
	}

	public long getAnimationDelay()
	{
		return animationDelay;
	}

	public int getCurrentFrameIndex()
	{
		return currentFrame;
	}

	public TextureRegion getCurrentFrame()
	{
		return animationFrames.get(currentFrame);
	}

	public void setAnimationDelay(long animDelay)
	{
		animationDelay = animDelay;
	}

	public void setCurrentFrame(int currentFrame)
	{
		this.currentFrame = currentFrame;
	}

	public void start()
	{
		started = true;
	}

	public void stop()
	{
		started = false;
	}

	public void update(float delta)
	{
		if (!started)
			currentFrame = 0;

		if (System.currentTimeMillis() - lastTextureSwitch >= animationDelay && started)
		{
			currentFrame = (currentFrame + 1) % animationFrames.size();
			lastTextureSwitch = System.currentTimeMillis();
		}
	}

}
