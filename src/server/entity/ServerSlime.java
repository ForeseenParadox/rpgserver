package server.entity;

import java.io.DataOutputStream;

import draconic.rpggame.net.PacketType;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.Node;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.Player;
import draconic.rpggame.world.entity.Slime;
import draconic.rpggame.world.entity.component.BasicPathFollower;

public class ServerSlime extends Slime
{
	private Node path;
	private BasicPathFollower pathFollower;
	private boolean serialize;

	public ServerSlime(World world, Vector2f position)
	{
		super(world, position);
		pathFollower = new BasicPathFollower(this, 1);
		addEntityComponent(pathFollower);
	}

	@Override
	public void serialize(DataOutputStream output, int header)
	{
		if (header != PacketType.ENTITY_UPDATE || serialize)
			super.serialize(output, header);
	}

	@Override
	public void setHealth(int health)
	{
		super.setHealth(health);
		serialize = true;
	}

	public void update(float delta)
	{
		super.update(delta);
		// find the closest player
		Player closestPlayer = (Player) getWorld().closestEntity(this, Player.class);

		if (closestPlayer != null && distance(closestPlayer) < 500)
		{
			path = getWorld().findPath(getTileX(), getTileY(), closestPlayer.getTileX(), closestPlayer.getTileY());

			if (path != null)
				path = path.next;

			pathFollower.setPath(path);
		}
		
		if (path != null)
			serialize = true;
		else
			serialize = false;
	}

}
