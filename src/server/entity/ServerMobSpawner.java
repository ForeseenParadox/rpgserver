package server.entity;

import java.util.ArrayList;
import java.util.List;

import draconic.rpggame.util.TimePoll;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.Entity;
import draconic.rpggame.world.entity.MobSpawner;
import draconic.rpggame.world.entity.PointEntity;
import server.ServerWorld;

public class ServerMobSpawner extends MobSpawner
{

	private TimePoll poll;
	private int maxActive;
	private List<Entity> active;

	public ServerMobSpawner(World world, Vector2f position, float radius, int mob, long delay)
	{
		super(world, position, radius, mob);

		poll = new TimePoll(delay);
		maxActive = 2;
		active = new ArrayList<Entity>();
	}

	public Vector2f randomPosition()
	{
		float randAngle = (float) (Math.random() * 2 * Math.PI);
		float randMag = (float) (Math.random() * getRadius());
		return new Vector2f(getPosition().getX() + (float) (Math.cos(randAngle) * randMag), getPosition().getY() + (float) (Math.sin(randAngle) * randMag));
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		int index = 0;
		while (index < active.size())
		{
			Entity e = active.get(index);
			if (!e.getWorld().containsEntity(e))
				active.remove(e);
			else
				index++;
		}

		try
		{
			if (poll.poll() && maxActive > active.size())
			{
				PointEntity e = (PointEntity) ServerEntityFactory.createEntity(getMob(), (ServerWorld) getWorld());
				e.setPosition(randomPosition());
				getWorld().addEntity(e);
				active.add(e);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
