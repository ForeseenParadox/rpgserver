package server.entity;

import java.io.DataOutputStream;
import java.util.Iterator;

import draconic.rpggame.item.ItemStack;
import draconic.rpggame.net.PacketType;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.Entity;
import draconic.rpggame.world.entity.ItemEntity;
import draconic.rpggame.world.entity.component.AcceleratorComponent;
import draconic.rpggame.world.entity.component.RotatorComponent;

public class ServerItemEntity extends ItemEntity
{

	private AcceleratorComponent accelerator;
	private RotatorComponent rotator;
	private boolean serialize;

	public ServerItemEntity(World world, Vector2f position)
	{
		super(world, position);
		initComponents();
	}

	public ServerItemEntity(World world, Vector2f position, ItemStack itemStack)
	{
		super(world, position, itemStack);
		initComponents();
	}

	public void initComponents()
	{
		accelerator = new AcceleratorComponent(this, 0.4f);
		rotator = new RotatorComponent(this);

		addEntityComponent(accelerator);
		addEntityComponent(rotator);
	}

	@Override
	public void serialize(DataOutputStream output, int header)
	{
		if (header == PacketType.ENTITY_ADD || serialize)
			super.serialize(output, header);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		if (accelerator.getVelocity().length() > 0)
			rotator.setRotationSpeed(5);
		else
			rotator.setRotationSpeed(0);

		Iterator<Entity> eList = getWorld().getEntityIterator();
		while (eList.hasNext())
		{
			Entity e = eList.next();
			if (e instanceof ServerPlayer)
			{
				ServerPlayer player = (ServerPlayer) e;

				if (player.distance(this) < 300)
				{
					float dx = player.getMidX() - getMidX(), dy = player.getMidY() - getMidY();
					Vector2f vec = new Vector2f(dx, dy).normalized().multiply(1f);
					accelerator.applyForce(vec);
					serialize = true;
				} else
				{
					serialize = false;
				}

				if (player.intersects(this))
				{
					if (player.getInventory().addItem(getItemStack().getItem(), getItemStack().getCount()))
						getWorld().removeEntity(this);
				}

			}
		}

	}

}
