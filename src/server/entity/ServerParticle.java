package server.entity;

import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.Particle;

public class ServerParticle extends Particle
{

	public ServerParticle(World world, Vector2f position)
	{
		super(world, position);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
		
		if (getTicksLived() >= getLife())
			getWorld().removeEntity(this);
	}

}
