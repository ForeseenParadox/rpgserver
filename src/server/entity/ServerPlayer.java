package server.entity;

import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.entity.Player;
import server.ServerWorld;

public class ServerPlayer extends Player
{

	public ServerPlayer(ServerWorld world, Vector2f position)
	{
		super(world, position);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

	}

}
