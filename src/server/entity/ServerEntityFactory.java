package server.entity;

import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.entity.BasicNPC;
import draconic.rpggame.world.entity.Entity;
import server.ServerWorld;

public class ServerEntityFactory
{

	public static Entity createEntity(int type, ServerWorld parent)
	{
		Entity result = null;
		if (type == Entity.SLIME)
		{
			result = new ServerSlime(parent, new Vector2f());
		} else if (type == Entity.PLAYER)
		{
			result = new ServerPlayer(parent, new Vector2f());
		} else if (type == Entity.PROJECTILE)
		{
			result = new ServerProjectile(parent, new Vector2f(), 1, new Vector2f());
		} else if (type == Entity.ITEM)
		{
			result = new ServerItemEntity(parent, new Vector2f());
		} else if (type == Entity.PARTICLE)
		{
			result = new ServerParticle(parent, new Vector2f());
		} else if (type == Entity.NPC)
		{
			result = new BasicNPC(parent, new Vector2f());
		}

		if (result == null)
			System.err.println("Entity type " + type + " not found! Unable to properly construct entity.");

		return result;

	}

}
