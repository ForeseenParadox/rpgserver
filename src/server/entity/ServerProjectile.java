package server.entity;

import java.util.Iterator;

import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.Entity;
import draconic.rpggame.world.entity.MobileEntity;
import draconic.rpggame.world.entity.Player;
import draconic.rpggame.world.entity.Projectile;
import draconic.rpggame.world.entity.SizedEntity;
import draconic.rpggame.world.entity.Slime;

public class ServerProjectile extends Projectile
{

	public ServerProjectile(World world, Vector2f position, int damage, float dir, float mag)
	{
		super(world, position, damage, dir, mag);
	}

	public ServerProjectile(World world, Vector2f position, int damage, Vector2f velocity)
	{
		super(world, position, damage, velocity);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		boolean entityCollision = false;
		SizedEntity collider = null;

		Iterator<Entity> entityIterator = getWorld().getEntityIterator();

		while (entityIterator.hasNext())
		{
			Entity e = entityIterator.next();
			if (e instanceof MobileEntity && !(e instanceof Player) && e != getFiring())
			{
				SizedEntity c = (SizedEntity) e;
				if (c.intersects(this))
				{
					collider = c;
					entityCollision = true;

					if (c instanceof Slime)
					{
						MobileEntity mob = (MobileEntity) c;
						mob.changeHealth(-getDamage());
					}
				}
			}
		}

		if (!canMove(0, 0))
		{
			getWorld().removeEntity(this);

			for (int i = 0; i < 3; i++)
				getWorld().addEntity(new ServerParticle(getWorld(), new Vector2f(getMidX(), getMidY()).mutateAdd(getVelocity())));
		}
		if (entityCollision || getLived() >= getLife())
			getWorld().removeEntity(this);
	}

}
