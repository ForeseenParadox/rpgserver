package server;

import java.awt.geom.Ellipse2D;

import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.Layer;
import draconic.rpggame.world.Tile;
import draconic.rpggame.world.World;
import draconic.rpggame.world.WorldLoader;
import server.entity.ServerMobSpawner;
import server.tiled.MapLayer;
import server.tiled.MapObject;
import server.tiled.ObjectLayer;
import server.tiled.TileLayer;
import server.tiled.TiledMap;

public class ServerWorldLoader extends WorldLoader
{

	@Override
	public World loadWorld(String worldName, String path)
	{

		TiledMap map = new TiledMap(path);
		int tileSize = map.getProperties().getTileWidth();
		ServerWorld result = new ServerWorld(worldName, tileSize, map);
		int layerCount = map.getLayerCount();
		for (int i = 0; i < layerCount; i++)
		{
			MapLayer mapLayer = map.getLayer(i);
			if (mapLayer instanceof TileLayer)
			{
				TileLayer layer = ((TileLayer) map.getLayer(i));

				Layer l = new Layer(layer.getWidth(), layer.getHeight());
				for (int x = 0; x < layer.getWidth(); x++)
					for (int y = 0; y < layer.getHeight(); y++)
					{
						if (layer.getTileAt(x, y) != null)
						{
							server.tiled.Tile t = layer.getTileAt(x, y);

							// TODO: USE CORRECT TILE IDS
							l.getTiles()[x + (layer.getHeight() - y - 1) * l.getWidth()] = new Tile(0);
						}
					}
				result.addLayer(layer.getName(), l);
			} else
			{
				ObjectLayer objects = (ObjectLayer) mapLayer;
				for (MapObject obj : objects.getObjects())
				{
					if (obj.getName().contains("Spawner"))
					{
						Ellipse2D ellipse = (Ellipse2D) obj.getShape();
						int mob = Integer.parseInt(obj.getProperty("mobType"));
						result.addEntity(new ServerMobSpawner(result, new Vector2f((float) ellipse.getCenterX(), tileSize * map.getProperties().getHeight() - (float) ellipse.getCenterY()), (float) ellipse.getWidth() / 2, mob, 500));
					}
				}
			}
		}

		return result;

	}

}
