package server.tiled;

import java.awt.Shape;
import java.util.HashMap;
import java.util.Map;

public class MapObject
{

	private String name;
	private Shape shape;

	private Map<String, String> properties;

	public MapObject(String name, Shape shape)
	{
		this.name = name;
		this.shape = shape;
		properties = new HashMap<String, String>();
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getProperty(String name)
	{
		return properties.get(name);
	}

	public void addProperty(String name, String value)
	{
		properties.put(name, value);
	}

	public Shape getShape()
	{
		return shape;
	}

	public void setShape(Shape shape)
	{
		this.shape = shape;
	}

}
