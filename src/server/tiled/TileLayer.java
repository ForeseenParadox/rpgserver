package server.tiled;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import javax.xml.bind.DatatypeConverter;

import com.google.common.io.LittleEndianDataInputStream;

import draconic.rpggame.util.xml.XMLElement;

/**
 * The Class TiledLayerTileLayer.
 */
public class TileLayer extends MapLayer
{

	/** The width. */
	private int width;

	/** The height. */
	private int height;

	/** The tiles. */
	private Tile[] tiles;

	private String encoding;

	private String compression;

	/**
	 * Instantiates a new tiled layer tile layer.
	 *
	 * @param properties
	 *            the properties
	 * @param tilesets
	 *            the tilesets
	 * @param rootElement
	 *            the root element
	 */
	public TileLayer(MapProperties properties, List<TileSet> tilesets, XMLElement rootElement)
	{
		super(properties, rootElement);
		width = Integer.parseInt(rootElement.getAttribValue("width"));
		height = Integer.parseInt(rootElement.getAttribValue("height"));
		tiles = new Tile[width * height];

		XMLElement dataElement = rootElement.getChildByName("data");
		encoding = dataElement.getAttribValue("encoding");
		compression = dataElement.getAttribValue("compression");

		int[] gId = new int[width * height];

		if (encoding != null)
		{
			String base64 = dataElement.getValue();
			byte[] data = DatatypeConverter.parseBase64Binary(base64);
			byte[] result = data;

			if (compression != null && compression.equals("zlib"))
			{
				try
				{
					Inflater decompressor = new Inflater();
					decompressor.setInput(data);
					result = new byte[width * height * 4];
					decompressor.inflate(result);
					decompressor.end();

				} catch (DataFormatException e)
				{
					e.printStackTrace();
				}
			}
			try
			{
				LittleEndianDataInputStream inputStream = new LittleEndianDataInputStream(new ByteArrayInputStream(result));
				for (int i = 0; i < gId.length; i++)
					gId[i] = inputStream.readInt();
				inputStream.close();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		} else
		{
			// uncompressed, xml data
			List<XMLElement> tileData = dataElement.getChildrenByName("tile");
			for (int i = 0; i < gId.length; i++)
				gId[i] = Integer.parseInt(tileData.get(i).getAttribValue("gid"));
		}

		int tileIndex = 0;
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				int gid = gId[tileIndex];

				if (gid > 0)
				{
					for (int i = 0; i < tilesets.size(); i++)
					{
						if (i == tilesets.size() - 1)
							tiles[tileIndex] = tilesets.get(i).getTile(gid);
						else
						{
							int thisGid = tilesets.get(i).getFirstGid(), nextGid = tilesets.get(i + 1).getFirstGid();
							if (gid >= thisGid && gid <= nextGid)
								tiles[tileIndex] = tilesets.get(i).getTile(gid);
						}
					}
				}

				tileIndex++;
			}
		}
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Gets the tile.
	 *
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @return the tile
	 */
	public Tile getTileAt(int x, int y)
	{
		return tiles[x + y * width];
	}

	/**
	 * Gets the tiles.
	 *
	 * @return the tiles
	 */
	public Tile[] getTiles()
	{
		return tiles;
	}

}
