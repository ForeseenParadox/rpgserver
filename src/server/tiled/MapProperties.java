package server.tiled;

import draconic.rpggame.util.xml.XMLElement;

/**
 * The Class TiledMapProperties.
 */
public class MapProperties
{

	/** The version. */
	private String version;

	/** The orientation. */
	private String orientation;

	/** The width. */
	private int width;

	/** The height. */
	private int height;

	/** The tile width. */
	private int tileWidth;

	/** The tile height. */
	private int tileHeight;

	/**
	 * Instantiates a new tiled map properties.
	 */
	public MapProperties()
	{
		this("", "", 0, 0, 0, 0);
	}

	/**
	 * Instantiates a new tiled map properties.
	 *
	 * @param root
	 *            the root
	 */
	public MapProperties(XMLElement root)
	{
		version = root.getAttribValue("version");
		orientation = root.getAttribValue("orientation");
		width = Integer.parseInt(root.getAttribValue("width"));
		height = Integer.parseInt(root.getAttribValue("height"));
		tileWidth = Integer.parseInt(root.getAttribValue("tilewidth"));
		tileHeight = Integer.parseInt(root.getAttribValue("tileheight"));
	}

	/**
	 * Instantiates a new tiled map properties.
	 *
	 * @param version
	 *            the version
	 * @param orientation
	 *            the orientation
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param tileWidth
	 *            the tile width
	 * @param tileHeight
	 *            the tile height
	 */
	public MapProperties(String version, String orientation, int width, int height, int tileWidth, int tileHeight)
	{
		this.version = version;
		this.orientation = orientation;
		this.width = width;
		this.height = height;
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(String version)
	{
		this.version = version;
	}

	/**
	 * Gets the orientation.
	 *
	 * @return the orientation
	 */
	public String getOrientation()
	{
		return orientation;
	}

	/**
	 * Sets the orientation.
	 *
	 * @param orientation
	 *            the new orientation
	 */
	public void setOrientation(String orientation)
	{
		this.orientation = orientation;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Sets the width.
	 *
	 * @param width
	 *            the new width
	 */
	public void setWidth(int width)
	{
		this.width = width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height
	 *            the new height
	 */
	public void setHeight(int height)
	{
		this.height = height;
	}

	/**
	 * Gets the tile width.
	 *
	 * @return the tile width
	 */
	public int getTileWidth()
	{
		return tileWidth;
	}

	/**
	 * Sets the tile width.
	 *
	 * @param tileWidth
	 *            the new tile width
	 */
	public void setTileWidth(int tileWidth)
	{
		this.tileWidth = tileWidth;
	}

	/**
	 * Gets the tile height.
	 *
	 * @return the tile height
	 */
	public int getTileHeight()
	{
		return tileHeight;
	}

	/**
	 * Sets the tile height.
	 *
	 * @param tileHeight
	 *            the new tile height
	 */
	public void setTileHeight(int tileHeight)
	{
		this.tileHeight = tileHeight;
	}

}
