package server.tiled;

import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;

import draconic.rpggame.util.xml.XMLElement;

/**
 * The Class TiledLayerObjectLayer.
 */
public class ObjectLayer extends MapLayer
{

	private int width;
	private int height;
	private List<MapObject> objects;

	public ObjectLayer(MapProperties properties, XMLElement rootElement)
	{
		super(properties, rootElement);

		if (rootElement.getAttrib("width") != null)
			width = Integer.parseInt(rootElement.getAttribValue("width"));

		if (rootElement.getAttrib("height") != null)
			height = Integer.parseInt(rootElement.getAttribValue("height"));

		objects = new ArrayList<MapObject>();

		List<XMLElement> objectElements = rootElement.getChildrenByName("object");
		for (XMLElement objectElement : objectElements)
		{
			MapObject object = null;
			float x = Float.parseFloat(objectElement.getAttribValue("x"));
			float y = Float.parseFloat(objectElement.getAttribValue("y"));
			float width = Integer.parseInt(objectElement.getAttribValue("width"));
			float height = Integer.parseInt(objectElement.getAttribValue("height"));
			String name = objectElement.getAttribValue("name");

			if (objectElement.getChildren().size() == 0)
			{

				// PolygonShape shape = new PolygonShape();
				// Vec2[] verts =
				// {
				// new Vec2(x, y),
				// new Vec2(x, y + height),
				// new Vec2(x + width, y + height),
				// new Vec2(x + width, y) };

				// for (int i = 0; i < verts.length; i++)
				// {
				// verts[i].y = mapHeight - verts[i].y;
				// verts[i] = verts[i].mul(1.0f / GameWorld.PPM);
				// System.out.println(verts[i].y);
				// }
				// shape.set(verts, verts.length);
				// shapes.add(shape);
			} else
			{
				List<String> childrenNames = objectElement.getChildrenNames();

				if (childrenNames.contains("ellipse"))
				{
					object = new MapObject(name, new Ellipse2D.Float(x, y, width, height));
				} else if (childrenNames.contains("polygon"))
				{
					// String[] pointsAsString =
					// childElement.getAttribValue("points").split(" ");
					// Vec2[] points = new Vec2[pointsAsString.length];
					//
					// for (int i = 0; i < points.length; i++)
					// {
					// String[] coordinateTokens = pointsAsString[i].split(",");
					// points[i] = new
					// Vec2((Float.parseFloat(coordinateTokens[0]) + x) /
					// GameWorld.PPM, (mapHeight -
					// (Float.parseFloat(coordinateTokens[1]) + y)) /
					// GameWorld.PPM);
					// }
					//
					// PolygonShape shape = new PolygonShape();
					// shape.set(points, points.length);
					// shapes.add(shape);
				} else if (childrenNames.contains("polyline"))
				{

				}
			}

			XMLElement propertiesElement = objectElement.getChildByName("properties");
			List<XMLElement> propertiesElements = propertiesElement.getChildrenByName("property");
			for (XMLElement z : propertiesElements)
				object.addProperty(z.getAttribValue("name"), z.getAttribValue("value"));
			objects.add(object);
		}

	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public List<MapObject> getObjects()
	{
		return objects;
	}

}
