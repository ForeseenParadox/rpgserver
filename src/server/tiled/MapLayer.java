package server.tiled;

import draconic.rpggame.util.xml.XMLElement;

/**
 * The Class TiledLayer.
 */
public abstract class MapLayer
{

	/** The name. */
	private String name;

	/**
	 * Instantiates a new tiled layer.
	 *
	 * @param properties the properties
	 * @param rootElement the root element
	 */
	public MapLayer(MapProperties properties, XMLElement rootElement)
	{
		name = rootElement.getAttribValue("name");
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

}