package server.tiled;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import draconic.rpggame.util.xml.XMLDocument;
import draconic.rpggame.util.xml.XMLElement;
import server.Texture;

/**
 * The Class TiledMap.
 */
public class TiledMap
{

	private File parent;
	private String location;

	/** The properties. */
	private MapProperties properties;

	/** The tilesets. */
	private List<TileSet> tilesets;

	/** The layers. */
	private List<MapLayer> layers;

	/**
	 * Instantiates a new tiled map.
	 *
	 * @param handle
	 *            the handle
	 */
	public TiledMap(String loc)
	{
		XMLDocument xml = null;
		XMLElement root = null;
		try
		{
			xml = new XMLDocument(new FileInputStream(loc));
			root = xml.getRootElement();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		location = loc;
		parent = new File(location).getParentFile();
		properties = new MapProperties(root);
		tilesets = new ArrayList<TileSet>();
		layers = new ArrayList<MapLayer>();

		List<XMLElement> ts = root.getChildrenByName("tileset");
		for (XMLElement child : ts)
		{
			String name = child.getAttribValue("name");
			int firstGid = Integer.parseInt(child.getAttribValue("firstgid"));
			int tileWidth = Integer.parseInt(child.getAttribValue("tilewidth"));
			int tileHeight = Integer.parseInt(child.getAttribValue("tileheight"));

			List<XMLElement> sources = child.getChildrenByName("image");

			// TODO: implement multi image tileset loading
			if (sources.size() > 1)
				throw new IllegalArgumentException("Tiled loader does not currently support multi image tilesets.");

			XMLElement source = sources.get(0);

			// image source
			String imageLocation = parent.getPath().replace('\\', '/') + "/" + source.getAttribValue("source");
			tilesets.add(new TileSet(name, new Texture(imageLocation), firstGid, tileWidth, tileHeight));
		}

		List<XMLElement> ls = root.getChildrenByName("layer");
		for (XMLElement l : ls)
		{
			TileLayer tlayer = new TileLayer(properties, tilesets, l);
			layers.add(tlayer);
		}

		List<XMLElement> os = root.getChildrenByName("objectgroup");
		for (XMLElement l : os)
		{
			ObjectLayer tlayer = new ObjectLayer(properties, l);
			layers.add(tlayer);
		}
	}

	public String getLocation()
	{
		return location;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public MapProperties getProperties()
	{
		return properties;
	}

	public int getLayerCount()
	{
		return layers.size();
	}

	public MapLayer getLayer(int i)
	{
		return layers.get(i);
	}

	/**
	 * Gets the tilesets.
	 *
	 * @return the tilesets
	 */
	public List<TileSet> getTilesets()
	{
		return tilesets;
	}

	/**
	 * Gets the layers.
	 *
	 * @return the layers
	 */
	public List<MapLayer> getLayers()
	{
		return layers;
	}

}