package server.tiled;

import server.TextureRegion;

/**
 * The Class TiledTile.
 */
public class Tile
{

	/** The texture. */
	private TextureRegion texture;

	/**
	 * Instantiates a new tiled tile.
	 *
	 * @param texture
	 *            the texture
	 */
	public Tile(TextureRegion texture)
	{
		this.texture = texture;
	}

	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}

	/**
	 * Sets the texture.
	 *
	 * @param texture
	 *            the new texture
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

}