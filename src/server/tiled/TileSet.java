package server.tiled;

import server.Texture;
import server.TextureRegion;

/**
 * The Class TiledTileSet.
 */
public class TileSet
{

	/** The name. */
	private String name;

	/** The texture. */
	private Texture texture;

	/** The first gid. */
	private int firstGid;

	/** The tile width. */
	private int tileWidth;

	/** The tile height. */
	private int tileHeight;

	/** The tiles. */
	private Tile[] tiles;

	/**
	 * Instantiates a new tiled tile set.
	 *
	 * @param name
	 *            the name
	 * @param texture
	 *            the texture
	 * @param firstGid
	 *            the first gid
	 * @param tileWidth
	 *            the tile width
	 * @param tileHeight
	 *            the tile height
	 */
	public TileSet(String name, Texture texture, int firstGid, int tileWidth, int tileHeight)
	{
		this.name = name;
		this.texture = texture;
		this.firstGid = firstGid;
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;

		int width = texture.getWidth() / tileWidth, height = texture.getHeight() / tileHeight;
		tiles = new Tile[width * height];

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				tiles[x + y * width] = new Tile(new TextureRegion(texture, x * tileWidth, texture.getHeight() - y * tileHeight - tileHeight, tileWidth, tileHeight));
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public Texture getTexture()
	{
		return texture;
	}

	/**
	 * Gets the first gid.
	 *
	 * @return the first gid
	 */
	public int getFirstGid()
	{
		return firstGid;
	}

	/**
	 * Gets the tile width.
	 *
	 * @return the tile width
	 */
	public int getTileWidth()
	{
		return tileWidth;
	}

	/**
	 * Gets the tile height.
	 *
	 * @return the tile height
	 */
	public int getTileHeight()
	{
		return tileHeight;
	}

	/**
	 * Gets the tile.
	 *
	 * @param gid
	 *            the gid
	 * @return the tile
	 */
	public Tile getTile(int gid)
	{
		return tiles[gid - firstGid];
	}

	/**
	 * Gets the tiles.
	 *
	 * @return the tiles
	 */
	public Tile[] getTiles()
	{
		return tiles;
	}

}