package server;

public class IdGen
{

	private volatile long counter;

	public IdGen()
	{
	}

	public synchronized long allocateId()
	{
		return counter++;
	}

}
