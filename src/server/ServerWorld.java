package server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;

import draconic.rpggame.item.ItemStack;
import draconic.rpggame.item.Items;
import draconic.rpggame.net.PacketType;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.BasicNPC;
import draconic.rpggame.world.entity.Entity;
import server.entity.ServerItemEntity;
import server.tiled.TiledMap;

public class ServerWorld extends World
{

	private TiledMap map;
	private Vector2f spawnPoint;

	public ServerWorld(String worldName, int tileSize, TiledMap map)
	{
		super(worldName, tileSize);
		this.map = map;
		this.spawnPoint = new Vector2f(45 * tileSize, 430 * tileSize);

		// for (int i = 0; i < 10; i++)
		for (int i = 0; i < 1000; i++)
		{
			addEntity(new ServerItemEntity(this, new Vector2f((int) (Math.random() * 200) * tileSize, (map.getProperties().getHeight() - (int) (Math.random() * 200)) * tileSize), new ItemStack(Items.getItem(Items.STICK), 1)));
		}

		addEntity(new BasicNPC(this, new Vector2f(50 * tileSize, 430 * tileSize)));
	}

	public TiledMap getMap()
	{
		return map;
	}

	public Vector2f getSpawnPoint()
	{
		return spawnPoint;
	}

	@Override
	public void addEntity(Entity e)
	{
		super.addEntity(e);

		// allocate entity id if not already
		if (!e.hasSerialId())
			e.assignSerialId(Server.getInstance().getEntityIdGen().allocateId());
		connectEntity(e);
	}

	@Override
	public void removeEntity(Entity e)
	{
		super.removeEntity(e);
		disconnectEntity(e);
	}

	public void serializeEntities(DataOutputStream output, int header)
	{
		Iterator<Entity> entityIterator = getEntityIterator();
		while (entityIterator.hasNext())
		{
			Entity e = entityIterator.next();
			if (header == PacketType.ENTITY_UPDATE && (e.getTypeId() == Entity.PROJECTILE || e.getTypeId() == Entity.PARTICLE))
			{

			} else
			{
				if (e != null)
					e.serialize(output, header);
			}
		}
	}

	private void connectEntity(Entity e)
	{
		e.serialize(Server.getInstance().getGlobalDataOut(), PacketType.ENTITY_ADD);
	}

	private void disconnectEntity(Entity e)
	{
		// remove player from all other clients
		try
		{
			Server.getInstance().getGlobalDataOut().writeInt(PacketType.ENTITY_REMOVE);
			Server.getInstance().getGlobalDataOut().writeLong(e.getSerialId());
		} catch (IOException e2)
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
	}

}
